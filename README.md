# Ammo Recovery

Post-combat ammo recovery.

![Ammo Recovery Usage](./img/screencaps/chatlog.png)

Optionally auto-deletes the recovery prompt message.

Other players won't see the recovery buttons, as they're only displayed to the owner (and GM by extension of that).

![Ammo Recovery As Seen By Other Players](./img/screencaps/others.png)

Additionally can hide the built-in recovery buttons during combat.

The recovery now supports manual adjustment. The setting is global GM-side setting. The manual adjustment can be inverted by holding shift when clicking a recovery button.

![Ammo Recovery Manual Input](./img/screencaps/query.png)

This allows you to ensure certain number of ammunition is destroyed or saved before the recovery chance is considered for what remains.

The card output will show manual adjustment and configuration formula effect on the recovery.  
![Ammo Recovery Result](./img/screencaps/result.png)

NOTE! Saving ammunition is prioritized over destroying.

## Rationale

Tarbarian mentioned something like this as useful and I thought so too. It's neat, it's here.  
I have expanded this since with additional features since I really liked it.  
Built-in ammo recovery is only useful outside of combat.

## Configuration

In module configuration GM can enable both recovery and message cleanup options.

![Ammo Recovery Configuration](./img/screencaps/config.png)

The recovery and destruction formula are based on initial quantity being saved, not in incremental steps, so same formula for both should result in same quantity, but the recovery formula is processed first and may overrule destruction formula as such for final results.

## API

```js
const amrapi = game.modules.get('koboldworks-pf1-ammo-recovery').api;

// Force ammo recovery
amrapi.recover(); // Defaults to currently selected tokens
amrapi.recover({actors:[...actors]}); // for for specific actors
amrapi.recover({tokens:[...tokens]}); // for for specific tokens
amrapi.recover({verbose:false}); // Do not display notifications
amrapi.recover({context:'CombatID'}); // Default to all contexts ('*'). Use "world" for out of combat use.

// Following functions have similar options as above, except for verbose and context are missing.

amrapi.purge(); // Purge recovery data
amrapi.migrate(); // Migrate recovery data
amrapi.query(); // Query recovery data
```

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/ammo-recovery/-/raw/latest/module.json>

Latest for 0.9.x: <https://gitlab.com/koboldworks/pf1/ammo-recovery/-/raw/1.2.1/module.json>
Latest for 0.7.x: <https://gitlab.com/koboldworks/pf1/ammo-recovery/-/raw/1.1.0.2/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
