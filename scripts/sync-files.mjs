/**
 * Sync static files
 */

import { watch } from 'fs/promises';
import { Glob } from 'bun';
import fs from 'node:fs';
import path from 'path';
import process from 'process';

const debug = false;

const DEST = './dist/';

const args = process.argv.slice(2);
const doWatch = args.includes('--watch');

const ignore = ['.git/**/*', 'scripts/**/*']
	.map(m => new Glob(m));

const mappings = [
	{ source: './static/*', target: '.' },
	{ source: './lang/**/*.json', target: '.' },
	{ source: './template/**/*.hbs', target: '.' },
	{ source: './*.md', target: '.' },
	{ source: './LICENSE', target: '.' },
	{ source: './CHANGELOG', target: '.' },
];

mappings.forEach(m => m.glob = new Glob(m.source));

const transformPath = (p) => p.replace(/^(\.[/\\])?static[/\\]/, '');

const syncFile = (npath, type, event) => {
	const spath = path.join('./', npath);
	const tpath = transformPath(npath);
	const dpath = path.join(DEST, tpath);

	if (type === 'rename') {
		// TODO: delete old
		// BUG: No knowledge of new name
		console.log('ren', ':', tpath);
		fs.unlink(dpath, (err) => {
			if (err) console.error('- failed to remove old file:', dpath);
		});
		return;
	}

	// Test if source file is newer
	try {
		if (fs.existsSync(dpath) && fs.statSync(spath).mtime <= fs.statSync(dpath).mtime) {
			if (debug) console.log('Old file:', tpath);
			return;
		}
		console.log(type, ':', tpath);
	}
	catch (err) {
		console.error(err);
		console.log(type, ':', spath, '->', dpath);
	}
	// TODO: Debounce
	fs.cpSync(spath, dpath, { recursive: true });
};

function syncMapping(m) {
	if (!m) return;
	for (const file of m.glob.scanSync()) {
		syncFile(file, 'add');
	}
}

for (const m of mappings) {
	syncMapping(m);
}

// Stop here if not watching
if (!doWatch) process.exit(0);

const watcher = watch('.', { recursive: true });

// close watcher when Ctrl-C is pressed
process.on('SIGINT', () => {
	console.log('Closing watcher...');
	watcher.close();
	process.exit(0);
});

for await (const event of watcher) {
	if (ignore.some(g => g.match(event.filename))) continue;
	const match = mappings.find(m => m.glob.match(event.filename));
	if (!match) {
		// Scan a directory on rename
		if (event.eventType === 'change' && fs.lstatSync(event.filename)?.isDirectory()) {
			const m = mappings.find(m => event.filename.startsWith(m.prefix));
			syncMapping(m);
		}
		continue;
	}

	syncFile(event.filename, event.eventType, event);
}
