# Change Log

## 3.1.0

- New: Support for abundant ammunition.
- Fix: Incorrect shift key detection.
- Foundry v12 & PF1 v11 support. Support for older dropped.

## 3.0.1

- Fix: Ammo recovery chance was not correctly retrieved, causing all to use default 50%

## 3.0.0.4

- Fix: Recovery buttons could sometimes recover 50% even if different recovery choice was selected.

## 3.0.0.2

- Fix: Clicking 0% recovery option would recover 100%

## 3.0.0

- Refactor: Most options made available directly in Foundry's settings dialog.
- Refactor: Heavy internal modifications. Bugs may have appeared.
- Removed: Some options were removed.
- Removed: Recovery chance configuration, as this is built-in to the system now.
- Foundry v11 support confirmed.
- Minimum PF1 version increased to 9.6

## 2.0.0

- Fix: Permission check was done using severely outdated constants.
- Change: i18n keys have changed to better organize them.
- Fix: Cancelling recovery dialog restores button functionality.
- Fix: Keeping recovery messages behaved poorly.
- New: Basic API added.
- Foundry v10 support, support for older versions dropped.

## 1.2.1

- Compatibility update with PF1's new built-in ammo recovery.  
  Clearing built-in recovery only removes the recovery buttons, not the used ammo display.

## 1.2.0 Foundry v9 compatibilty

- Foundry 0.7 compatibility removed

## 1.1.1.3 Maintenance update

## 1.1.1.2 Foundry 0.7.x cross-compatibility

## 1.1.1.1

- Fix: Permission checking was not done correctly.

## 1.1.1

- Fix: Styling of query and result chat cards when the module is disabled.
- Note: FoundryVTT 0.8.x compatibility.

## 1.1.0.2 Hotfix for PF1#782

## 1.1.0.1 Hotfix for null reference

## 1.1.0 Card consistency

- Changed: Cards no longer respect selected roll mode but rather configuration in module settings.
- Added: Option to keep NPC cards secret regardless of the above setting.

## 1.0.0 Dedicated module

- Fix: Indirect fix to ammo recovery not being availble on refresh.
