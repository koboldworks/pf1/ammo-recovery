import { CFG } from './common.mjs';
import { AmmoRecoveryConfig } from './config.mjs';

Hooks.once('init', function registerSettings() {
	game.settings.register(CFG.id, CFG.SETTINGS.autoDelete, {
		name: 'Koboldworks.AmmoRecovery.Settings.AutoDeleteLabel',
		hint: 'Koboldworks.AmmoRecovery.Settings.AutoDeleteHint',
		scope: 'world',
		type: Boolean,
		default: true,
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.alwaysQuery, {
		name: 'Koboldworks.AmmoRecovery.Settings.AutoQueryLabel',
		hint: 'Koboldworks.AmmoRecovery.Settings.AutoQueryHint',
		scope: 'world',
		type: Boolean,
		default: false,
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.defaultChances, {
		name: 'Koboldworks.AmmoRecovery.Settings.DefaultChancesLabel',
		hint: 'Koboldworks.AmmoRecovery.Settings.DefaultChancesHint',
		scope: 'world',
		type: Boolean,
		default: false,
		config: true,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.formulas, {
		scope: 'world',
		type: Object,
		default: { save: null, destroy: null },
		config: false,
	});

	game.settings.register(CFG.id, CFG.SETTINGS.disableBuiltin, {
		name: 'Koboldworks.AmmoRecovery.Settings.NoBuiltinLabel',
		hint: 'Koboldworks.AmmoRecovery.Settings.NoBuiltinHint',
		scope: 'world',
		type: Boolean,
		default: true,
		config: true,
	});

	game.settings.registerMenu(CFG.id, CFG.SETTINGS.formulas, {
		id: 'koboldworks-ammo-recovery-config',
		name: 'Koboldworks.AmmoRecovery.Settings.Formulas.Label',
		label: 'Koboldworks.AmmoRecovery.Settings.Formulas.Button',
		hint: 'Koboldworks.AmmoRecovery.Settings.Formulas.Hint',
		icon: 'fas fa-recycle',
		type: AmmoRecoveryConfig,
		restricted: true,
	});
});
