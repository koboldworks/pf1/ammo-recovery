import { CFG, getSetting } from './common.mjs';

export class AmmoRecoveryConfig extends FormApplication {
	constructor(object, options) {
		super(object, options);

		this.recovery = getSetting(CFG.SETTINGS.formulas);
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			id: 'koboldworks-ammo-recovery-config',
			classes: [...options.classes, 'koboldworks', 'koboldworks-ammo-recovery-config'],
			title: game.i18n.localize('Koboldworks.AmmoRecovery.Title'),
			template: CFG.templates.config,
			width: 480,
			closeOnSubmit: true,
		};
	}

	getData() {
		return {
			recovery: this.recovery,
		};
	}

	_onResetDefaults(event) {
		event.preventDefault();

		this.recovery.destroyFormula = '';
		this.recovery.saveFormula = '';

		ui.notifications.warn('You need to save changes to commit the reset.');

		this.render();
	}

	activateListeners(html) {
		super.activateListeners(html);

		html.find('button[name="reset"]').click(this._onResetDefaults.bind(this));
	}

	async _updateObject(_, formData) {
		const { save, destroy } = foundry.utils.expandObject(formData).recovery;

		const rollData = { quantity: 100, chanceFlat: 50, chance: 0.5 };
		let doUpdate = true;

		async function testFormula(formula) {
			try {
				await new Roll.defaultImplementation(formula, rollData).evaluate();
			}
			catch (err) {
				doUpdate = false;
				console.error('AMMO RECOVERY | Formula error:', formula, err);
			}
		}

		if (save?.length > 0)
			testFormula(save);
		if (destroy?.length > 0)
			testFormula(destroy);

		this.recovery = { save, destroy };
		if (doUpdate) {
			game.settings.set(CFG.id, CFG.SETTINGS.formulas, this.recovery);
			this.render();
		}
	}
}
