export const CFG = /** @type {const} */ ({
	id: 'koboldworks-pf1-ammo-recovery',
	SETTINGS: {
		formulas: 'formulas',
		autoDelete: 'autoDelete',
		disableBuiltin: 'disableBuiltin',
		alwaysQuery: 'alwaysQuery',
		defaultChances: 'defaultChances',
		save: 'save',
		destroy: 'destroy',
	},
	templates: {
		dialog: undefined, // Manual adjust dialog
		query: undefined, // Query card
		result: undefined, // Result card
		config: undefined, // Config application
	},
	debug: false,
	messages: [],
});

export const getSetting = (key) => game.settings.get(CFG.id, key);

const templatePath = (file) => `modules/${CFG.id}/template/${file}.hbs`;
Object.keys(CFG.templates).forEach(k => CFG.templates[k] = templatePath(k));

Hooks.once('setup', () => loadTemplates(Object.values(CFG.templates)));
