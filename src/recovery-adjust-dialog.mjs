import { CFG } from './common.mjs';

export class RecoveryAdjustDialog extends Dialog {
	constructor(data, options = {}) {
		super(data, options);
		this.rdata = data.recovery;
	}

	/**
	 *
	 * @param {object} rdata
	 * @param {object} options
	 * @param {Event} options.event
	 * @returns {Promise}
	 */
	static async open(rdata, { event } = {}) {
		const content = await renderTemplate(CFG.templates.dialog, rdata);

		return RecoveryAdjustDialog.open({
			content,
			title: game.i18n.localize('Koboldworks.AmmoRecovery.Query.Title'),
			buttons: {
				okay: {
					label: game.i18n.localize('Koboldworks.AmmoRecovery.Query.Verb'),
					icon: '<i class="far fa-hand-paper"></i>',
					callback: (html) => new FormDataExtended(html.querySelector('form')).object,
				},
			},
			default: 'okay',
			close: () => null,
		},
		{
			jQuery: false,
			height: 'auto',
			width: 'auto',
		});
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: ['dialog', 'koboldworks', 'ammo-recovery'],
			height: 140,
			width: 'auto',
			// scrollY: ['.overflowContent'],
			// closeOnSubmit: true,
			jQuery: true,
			// resizable: true,
		};
	}
}
