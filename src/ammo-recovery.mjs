import { CFG, getSetting } from './common.mjs';
import './settings.mjs';
import { RecoveryAdjustDialog } from './recovery-adjust-dialog.mjs';

// TODO: Interface for GM to control recovery.
//       Something like scroll box to mark mandatory destruction and toggle for enabling buttons. Option to just not require confirmation.

/**
 * @param {object} p
 * @param {Actor[]} p.actors
 * @param {TokenDocument[]} p.tokens
 * @returns {Actor[]} List of actors
 */
const getRelevantActors = ({ actors, tokens } = {}) => (actors ?? tokens?.map(t => t.actor) ?? canvas.tokens.controlled?.map(t => t.actor))
	?.filter(a => !!a);

/**
 * Return ammo items owned by the actor.
 *
 * @param {Actor} actor
 * @returns {Item[]} Ammo items
 */
const getAmmoItems = (actor) => actor.itemTypes.loot.filter(i => i.system.subType === 'ammo');

const getRelevantUsers = (doc) => game.users.players
	.filter(user => doc.testUserPermission(user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER))
	.sort((a, b) => a.id.localeCompare(b.id));

/**
 * @param {Actor} actor
 * @param {Item} ammo
 * @param {number} potential
 * @param {boolean} create Create card immediately
 */
async function createRecoveryCard({ actor, ammo, potential = 0, context = 'world' } = {}) {
	if (potential == 0 || ammo.system.abundant) {
		// TODO: Delete recovery data
		return;
	}

	const msgData = createStubMessage(actor);

	msgData.flags[CFG.id] = {
		recovered: false,
		context,
		potential,
		ammo: ammo.uuid,
	};

	const templateData = { actor, item: ammo, shot: potential, context };
	msgData.content = await renderTemplate(CFG.templates.query, templateData);

	return msgData;
};

function createStubMessage(actor) {
	const secret = !actor.hasPlayerOwner && actor.ownership.default < CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER;
	const rollMode = secret ? CONST.DICE_ROLL_MODES.PRIVATE : CONST.DICE_ROLL_MODES.PUBLIC;

	let userId = game.user.id;
	// Spoof user if created by GM
	if (game.user.isGM && actor.hasPlayerOwner) {
		// Check character config and then all owners otherwise.
		const user = game.users.players.find(u => u.character === actor);
		if (user) userId = user.id;
		else {
			const validOwners = getRelevantUsers(actor);
			if (validOwners.length)
				userId = validOwners[0].id;
		}
	}

	const msgData = {
		style: CONST.CHAT_MESSAGE_TYPES.OTHER,
		author: userId,
		speaker: ChatMessage.implementation.getSpeaker({ actor }),
		flags: { core: { canPopout: false }, [CFG.id]: {} },
	};

	ChatMessage.implementation.applyRollMode(msgData, rollMode);

	return msgData;
}

/**
 * User pressed recovery button.
 *
 * @param {PointerEvent} event - Triggering event
 * @param {ChatMessage} cm - Chat message
 * @param {Element} element - Button
 * @returns {void}
 */
async function onRecoveryButton(event, cm, element) {
	event.preventDefault();
	event.stopPropagation();

	let extendedQuery = getSetting(CFG.SETTINGS.alwaysQuery);
	if (event.shiftKey) extendedQuery = !extendedQuery;

	const button = element,
		card = button.closest('.message-content');

	const buttons = card.querySelectorAll('button');
	const toggleButtons = (state) => buttons.forEach(el => {
		el.disabled = !state;
		el.classList.toggle('hidden', !state);
	});

	toggleButtons(false);

	const actor = game.actors.get(cm.speaker.actor);

	// Figure out what to roll
	const potential = cm.getFlag(CFG.id, 'potential'),
		ammoUuid = cm.getFlag(CFG.id, 'ammo'),
		context = cm.getFlag(CFG.id, 'context') ?? 'world',
		item = fromUuidSync(ammoUuid);

	let chance = parseInt(button.dataset.chance) ?? 50;
	if (Number.isNaN(chance)) chance = 50;

	if (!item) return void ui.notifications.warn('Related ammo item not found');

	let recoveryPotential = potential,
		savedByFormula = 0,
		savedManual = 0,
		destroyedByFormula = 0,
		destroyedManual = 0;

	if (extendedQuery) {
		const rdata = { actor, item, potential, chance, source: { card, message: cm } };
		const fd = await RecoveryAdjustDialog.open(rdata, { event });
		if (!fd) {
			// Re-enable buttons
			toggleButtons(true);
			return void console.log('Ammo Recovery | Recovery Dialog cancelled');
		}

		savedManual = Math.clamped(fd.save || 0, 0, potential);
		destroyedManual = Math.clamped(fd.destroy || 0, 0, potential);
	}

	const allLost = chance === 0,
		doRoll = chance < 100,
		doCoins = chance === 50;

	const rollData = { quantity: recoveryPotential, chanceFlat: chance, chance: chance / 100 };

	recoveryPotential -= savedManual + destroyedManual;

	const { save, destroy } = getSetting(CFG.SETTINGS.formulas);

	let recoveredRoll = null;
	if (recoveryPotential > 0 && save?.length > 0) {
		try {
			recoveredRoll = await new Roll.defaultImplementation(save, rollData).evaluate();
			savedByFormula = recoveredRoll.total ?? 0;
			recoveryPotential -= savedByFormula;
		}
		catch (err) {
			console.error('AMMO RECOVERY | Recovery formula error: ', save, err);
			ui.notifications.error(`Ammo recovery formula error with "${item.name}".`, { permanent: true });
		}
	}
	let destroyedRoll = null;
	if (recoveryPotential > 0 && destroy?.length > 0) {
		try {
			destroyedRoll = await new Roll.defaultImplementation(destroy, rollData).evaluate();
			destroyedByFormula = destroyedRoll.total ?? 0;
			recoveryPotential -= destroyedByFormula;
		}
		catch (err) {
			console.error('AMMO RECOVERY | Destruction formula error: ', destroy, err);
			ui.notifications.error(`Ammo destruction formula error with "${item.name}"`, { permanent: true });
		}
	}

	rollData.dF = destroyedByFormula;
	rollData.dM = destroyedManual;
	rollData.sF = savedByFormula;
	rollData.sM = savedManual;

	const fQuantity = 'max(0, @quantity - @dF[DF] - @dM[DM] - @sF[SF] - @sM[SM])';
	// allLost = d0, doCoins = Xdc, !doCoins = d100, !doRoll = d1
	const fDice = allLost ? 'd0' : doRoll ? doCoins ? 'dc' : 'd100cs<=(@chanceFlat[Chance])' : 'd1';
	const recoveryFormula = `(${fQuantity})${fDice} + @sF[SF] + @sM[SM]`;

	let roll = null;
	try {
		roll = await new Roll.defaultImplementation(recoveryFormula, rollData).evaluate();
	}
	catch (err) {
		console.error('AMMO RECOVERY | Formula Error: ', recoveryFormula, rollData, err);
		return;
	}

	const actualRecovery = roll?.total ?? 0;
	const oldQuantity = item.system.quantity;

	// Actually recover the ammo
	try {
		// item.addCharges() would be preferable, but it doesn't allow returning the update data.
		await item.update({
			system: { quantity: oldQuantity + actualRecovery },
			flags: { [CFG.id]: { [`-=${context}`]: null } },
		});
	}
	catch (err) {
		console.error('Ammo Recovery error\n', err);
	}

	const dr = roll.dice[0].results,
		rollS = dr.filter(o => doCoins ? o.result : o.success).length,
		rollF = dr.length - rollS;

	// Generate message
	const templateData = {
		chance: chance,
		quantity: potential,
		item: item,
		roll: {
			data: roll,
			json: escape(JSON.stringify(roll)),
			formula: roll?.formula ?? '',
			result: roll?.total ?? potential,
			saved: rollS,
			destroyed: rollF,
		},
		saved: {
			roll: {
				enabled: save?.length > 0,
				result: savedByFormula,
				json: recoveredRoll ? escape(JSON.stringify(recoveredRoll)) : null,
				formula: save,
			},
			query: savedManual,
			total: savedByFormula + savedManual,
		},
		destroyed: {
			roll: {
				enabled: destroy?.length > 0,
				result: destroyedByFormula,
				json: destroyedRoll ? escape(JSON.stringify(destroyedRoll)) : null,
				formula: destroy,
			},
			query: destroyedManual,
			total: destroyedByFormula + destroyedManual,
		},
		total: {
			saved: actualRecovery,
			destroyed: potential - actualRecovery,
		},
	};

	console.log(`AMMO RECOVERY | Actor: ${actor.name} | ${chance}% | Ammo +${actualRecovery} to ${item.name} (${oldQuantity} -> ${item.system.quantity})`,
		`\nRecovered by: Roll(${templateData.roll.saved}), Formula(${savedByFormula}), Query(${savedManual})`,
		`\nDestroyed by: Roll(${templateData.roll.destroyed}), Formula(${destroyedByFormula}), Query(${destroyedManual})`,
	);

	const msgData = createStubMessage(actor);
	msgData.content = await renderTemplate(CFG.templates.result, templateData);

	// Self-destruct the query
	if (getSetting(CFG.SETTINGS.autoDelete)) {
		cm.delete();
	}
	else {
		cm.setFlag(CFG.id, 'recovered', true);
	}

	ChatMessage.implementation.create(msgData);
}

/**
 * @param {ChatMessage} cm
 * @param {HTMLElement} html
 * @param {Item} item
 */
function clearBuiltinRecovery(cm, html, item) {
	// TODO: PF1v10, use card combat reference

	// Do nothing if combat is not active
	if (!game.combat) return;

	const actor = item.actor;
	if (!actor) return;

	// This actor is not in combat
	if (!game.combat.combatants.some(c => c.actor === actor)) return;

	html.querySelectorAll('.chat-attack .ammo[data-ammo-id]')
		.forEach(el => {
			// el.remove()
			// Non-destructive removal
			el.style.display = 'none';
		});
}

/**
 * Adds recovery buttons to ammorecovery card.
 *
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function onChatMessage(cm, [html]) {
	const recovered = cm.getFlag(CFG.id, 'recovered');
	const isRecoveryMessage = recovered !== undefined;

	if (!isRecoveryMessage) {
		// Unrelated message?
		// Don't proceed unless appropriate setting is set.
		const removeBuiltin = getSetting(CFG.SETTINGS.disableBuiltin);
		if (!removeBuiltin) return;

		const item = cm.itemSource;
		if (!item?.isOwner) return;

		clearBuiltinRecovery(cm, html, item);
	}
	else {
		const el = isRecoveryMessage ? html.querySelector('.koboldworks .ammo-recovery') : null;

		// Recovery message, already processed or not actually one
		if (recovered) return console.log('AMMO RECOVERY | Already recovered');

		if (el.classList.contains('recovery-result')) return console.log('AMMO RECOVERY | No result found');

		// Actual ammo recovery message

		const bEl = el.querySelector('.buttons');

		// Hide warning about no longer being possible
		bEl.firstElementChild?.classList.add('hidden');

		const ammoUuid = cm.getFlag(CFG.id, 'ammo');
		const ammo = fromUuidSync(ammoUuid);
		if (!ammo) {
			console.log('AMMO RECOVERY | Used ammo not found', ammoUuid, cm);
			return;
		}

		const abundant = ammo.system.abundant ?? false;

		const baseChance = ammo?.system.recoverChance ?? 50;
		const chances = [];
		if (abundant)
			chances.push(0);
		if (getSetting(CFG.SETTINGS.defaultChances))
			chances.push(...Array.from(new Set([0, baseChance, 100])));
		else
			chances.push(baseChance);

		chances.forEach(chance => {
			const btn = document.createElement('button');
			btn.type = 'button';
			btn.dataset.chance = `${chance}`;
			btn.innerHTML = `<i class='fas fa-dice'></i> ${chance}%`;
			bEl.append(btn);
			// Hook buttons
			btn.addEventListener('click', ev => onRecoveryButton(ev, cm, btn));
		});
	}
}

/**
 * @param {Combat} combat
 */
const endCombatState = (combat) => {
	if (!game.users.activeGM?.isSelf) return;

	const actors = combat.combatants
		.map(c => c.actor)
		.filter(a => ['character', 'npc'].includes(a?.type));

	queryAmmoRecovery(actors, combat.id);
};

/**
 *
 * @param {object} usage Ammo usage
 * @param {object} options
 * @param {Actor} options.actor Triggering Actor
 * @param {Item} options.item Triggering Item
 * @param {Action} options.action Triggering Action
 * @param options.combat
 */
function actionUseHandlerEx(usage, { actor, item, action, combat } = {}) {
	const contextId = combat?.id ?? 'world';
	const fullUpdateData = Object.entries(usage)
		.map(([itemId, usageData]) => {
			const ammo = usageData.item;
			if (usageData.abundant) return null;

			const oldData = ammo.getFlag(CFG.id, contextId) ?? {};

			const ammoData = { count: (oldData.count ?? 0) + usageData.count };
			if (oldData.time === undefined) ammoData.time = game.time.worldTime;

			return { _id: itemId, flags: { [CFG.id]: { [contextId]: ammoData } } };
		})
		.filter(ammo => !!ammo);

	// Record ammo usage
	console.debug('Ammo Recovery | Recording:', fullUpdateData);
	return actor.updateEmbeddedDocuments('Item', fullUpdateData);
};

/**
 * Track action use and record used ammo.
 *
 * @param  {pf1.actionUse.ActionUse} au
 */
function actionUseHandler(au) {
	const { action, item, shared, actor } = au;

	if (actor.token) return; // Don't track unlinked tokens

	const inCombat = game.combat?.combatants.some(c => c.token?.actor?.id === actor.id) ?? false,
		combat = inCombat ? game.combat : null,
		ammoUsage = {};

	for (const atk of shared.attacks) {
		const ammoData = atk.chatAttack?.ammo;
		if (!ammoData) continue;
		const { quantity, id: ammoId, name, img } = ammoData;
		const ammo = actor.items.get(ammoId);
		if (!ammo) continue;
		ammoUsage[ammoId] ??= { count: 0, name, img, item: ammo, abundant: ammo.system.abundant ?? false };
		ammoUsage[ammoId].count += ammoData.quantity;
	}

	actionUseHandlerEx(ammoUsage, { actor, item, action, combat });
}

/**
 * Force recovery for defined actors or tokens, or for currently selected tokens.
 *
 * For API only.
 *
 * @param {object} p
 * @param {Actor[]} p.actors
 * @param {TokenDocument[]} p.tokens
 * @param {string} p.context Context for recovery: * for all, 'world' for out of combat, or combat ID.
 * @param p.verbose
 */
async function performAmmoRecovery({ actors, tokens, context = '*', verbose = true } = {}) {
	if (verbose) ui.notifications.info('Forcing ammo recovery');
	actors = getRelevantActors({ actors, tokens });

	if (actors?.length == 0) return void (verbose ? ui.notifications.error('No actors found to recover ammo for.') : null);

	const msgs = [];

	for (const actor of actors) {
		const ammos = getAmmoItems(actor);
		for (const ammo of ammos) {
			if (ammo.system.abundant) continue;
			if (context == '*') {
				// eslint-disable-next-line no-shadow
				for (const [context, usage] of Object.entries(ammo.flags[CFG.id] ?? {})) {
					const potential = usage.count;
					if (potential > 0)
						msgs.push(createRecoveryCard({ actor, ammo, potential, context }));
				}
			}
			else {
				const usage = ammo.getFlag(CFG.id, context) ?? { count: 0 };
				const potential = usage.count;
				if (potential > 0)
					msgs.push(createRecoveryCard({ actor, ammo, potential, context }));
			}
		}
	}

	if (msgs.length)
		return ChatMessage.implementation.create(await Promise.all(msgs));
};

/**
 * @param {Actor[]} actors
 * @param {string} context Context ID ('world' or combat ID)
 */
async function queryAmmoRecovery(actors = [], context) {
	if (!context) return void console.error('Ammo Recovery | Query attempt with no context:', { context, actors });
	if (!actors.length) return void console.error('Ammo Recovery | Query attempt with no actors:', { context, actors });

	const msgs = [];
	const clearRecord = [];
	for (const actor of actors) {
		const ammos = getAmmoItems(actor);
		for (const ammo of ammos) {
			const usage = ammo.getFlag(CFG.id, context);
			if (usage === undefined) continue;
			const potential = usage?.count ?? 0;
			if (potential > 0)
				msgs.push(createRecoveryCard({ actor, ammo, potential, context }));
			clearRecord.push(ammo.id);
		}

		// Clear usage flags
		actor.updateEmbeddedDocuments('Item', clearRecord.map(_id => ({ _id, flags: { [`-=${CFG.id}`]: null } })));
	}

	if (msgs.length)
		return ChatMessage.implementation.create(await Promise.all(msgs));
}

/**
 * Delete recovery data for defined actors or tokens, or for currently selected tokens.
 *
 * For API only.
 *
 * @param {object} p
 * @param {Actor[]} p.actors
 * @param {TokenDocument[]} p.tokens
 */
async function purgeRecoveryData({ actors, tokens } = {}) {
	ui.notifications.info('Deleting recovery data');
	actors = getRelevantActors({ actors, tokens });

	const promises = [];

	for (const actor of actors) {
		const updates = [];
		const ammos = getAmmoItems(actor);
		for (const ammo of ammos) {
			if (ammo.flags[CFG.id])
				updates.push({ _id: ammo.id, flags: { [`-=${CFG.id}`]: null } });
		}

		if (updates.length) {
			console.debug('Ammo Recovery | Deleting recovery data:', { id: actor.id, name: actor.name, actor });
			const p = actor.updateEmbeddedDocuments('Item', updates)
				.catch(e => console.error(e));
			promises.push(p);
		}
	}

	await Promise.allSettled(promises);
	console.log('Ammo Recovery | All recovery data purged.');
};

/**
 * Delete recovery data for defined actors or tokens, or for currently selected tokens.
 *
 * For API only.
 *
 * @param {object} options
 * @param {Actor[]} options.actors
 * @param {TokenDocument[]} options.tokens
 */
async function migrateRecoveryData({ actors, tokens } = {}) {
	actors = getRelevantActors({ actors, tokens });

	if (actors.length == 0) return void ui.notifications.warn('No valid actors found for migration.');

	ui.notifications.info('Recovery data migration starting.');

	console.log('Migrating actors:', actors);

	const flagId = 'ammoRecovery';
	for (const actor of actors) {
		const ammoStacks = actor.getFlag(CFG.id, flagId);
		if (!ammoStacks) continue;

		delete ammoStacks.count;
		const itemUpdates = [];

		for (const [ammoId, stackData] of Object.entries(ammoStacks)) {
			const ammo = actor.items.get(ammoId);
			if (ammo?.system.abundant) {
				itemUpdates.push({ _id: ammo.id, flags: { [`-=${CFG.id}`]: null } });
				continue;
			}
			if (stackData.count > 0 && ammo) {
				const oldAmmo = ammo.getFlag(CFG.id, flagId) ?? { count: 0 };
				itemUpdates.push({ _id: ammo.id, flags: { [CFG.id]: { world: { count: oldAmmo.count + stackData.count } } } });
			}
		}

		if (itemUpdates.length)
			await actor.updateEmbeddedDocuments('Item', itemUpdates);
		await actor.unsetFlag(CFG.id, flagId);
		console.log(`Migrated: ${actor.name} [${actor.id}]`);
	}

	ui.notifications.info('Recovery data migration finished!');
};

/**
 * Query recovery data.
 *
 * For API only.
 *
 * @param {object} options
 * @param {Actor[]} options.actors
 * @param {TokenDocument[]} options.tokens
 */
function queryRecoveryData({ actors, tokens } = {}) {
	actors = getRelevantActors({ actors, tokens });
	const recoveryData = [];

	for (const actor of actors) {
		const stacks = [];
		const ammos = getAmmoItems(actor);
		for (const ammo of ammos) {
			const data = ammo.flags[CFG.id];
			if (data) stacks.push({ id: ammo.id, item: ammo, data });
		}
		if (stacks.length)
			recoveryData.push({ actor, recoveryData: stacks });
	}

	return recoveryData;
};

Hooks.on('deleteCombat', endCombatState);
Hooks.on('pf1PreDisplayActionUse', actionUseHandler);

function preInitMessageCollector(cm, html) {
	CFG.messages.push([cm, html]);
}

// Deal with delayed availability
Hooks.on('renderChatMessage', preInitMessageCollector);
Hooks.once('setup', function postInit() {
	Hooks.on('renderChatMessage', onChatMessage);
	Hooks.off('renderChatMessage', preInitMessageCollector);
	CFG.messages.forEach(([cm, html]) => onChatMessage(cm, html));
	delete CFG.messages;
});

Hooks.once('ready', () => {
	const mod = game.modules.get(CFG.id);

	mod.api = {
		recover: performAmmoRecovery,
		purge: purgeRecoveryData,
		migrate: migrateRecoveryData,
		query: queryRecoveryData,
	};

	console.log(`Ammo Recovery | ${mod.version} | Ready!`);
});
